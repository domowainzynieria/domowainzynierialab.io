---
Title: Czasem warto kwestionować standardy - moja przygoda z klawiaturą split
Date: 2024-01-12
---

Skończyłem technikum informatyczne, korzystam wyłącznie z Linuxa i okazjonalnie zajmuję się hobbystycznym, niezbyt umiejętnym programowaniem. Od jakichś 10 lat większość mojego życia skupia się wokół komputerów. Mimo to jeszcze kilka miesięcy temu nie potrafiłem pisać na klawiaturze. Tak poprawnie pisać - niepatrzenie na klawiaturę, palce wskazujące na F i J, i tak dalej. Mimo że ta umiejętność nie jest aż tak powszechna, jakby można sądzić po naoglądaniu się zbyt wielu programistycznych influencerów, to i tak było mi trochę wstyd, że jej nie posiadam. Tak więc gdy nadeszły wakacje, postanowiłem skorzystać z przerwy od studiów i rzeczoną umiejętność posiąść.

Wiedziałem, że zadanie to nie będzie proste, ze względu na mój całkowity brak koordynacji. Niemniej jednak dzielnie trałem w regularnych, aczkolwiek niezbyt intensywnych ćwiczeniach. Szybko jednak dostrzegłem pewien bardzo istotny problem, którego do tej pory nigdy nie zauważałem. Standardowa klawiatura jest strasznie niewygodna. Problemów jest wiele, ale dla mnie najbardziej uciążliwą wadą jest przesunięcie wierszy. Jest to cecha kompletnie zbędna, która powstała po to, aby maszyny do pisania miały prostszą konstrukcję, a pozostała tylko dlatego, że wszyscy są do niej przyzwyczajeni.

W tym samym czasie, ponieważ Google wie o swoich użytkownikach rzeczy, które użytkownicy zdążyli jedynie pomyśleć, zacząłem się na YouTubie mimowolnie zagłębiać w świat niestandardowych klawiatur. Po kilku tygodniach wreszcie zdecydowałem się sprawić sobie taki wynalazek. Wybór padł na [Ferris Sweep](https://github.com/davidphilipbarr/Sweep), minimalistyczną, open-source'ową klawiaturę typu split.

### Spis treści:

- [Hardware](#hardware)
- [Software](#software)
  - [Lokalna kompilacja](#lokalna-kompilacja)
- [Konfigurowanie ZMK](#konfigurowanie-zmk)
- [Dotychczasowe wrażenia](#dotychczasowe-wrażenia)

## Hardware

Klawiatura jest dostępna w kilku wersjach, posiadających różne, niekompatybilne ze sobą cechy. Ja się zdecydowałem na wersję "Half Swept", która od standardowego wariantu różni się odwracalną płytką, pozwalającą na zmniejszenie kosztu (zamiast dwóch osobnych płytek można zamówić dwa egzemplarze tej samej, co zazwyczaj jest tańsze). Oprócz tego użyłem też czerwonych, nisko-profilowych przełączników Kailh Choc, klawiszy MKB, listew precyzyjnych SIP, do zamontowania mikrokontrolerów, dwóch przełączników MSK 12C02, dwóch przycisków reset B3U-1000P(M) i dwóch mikrokontrolerów nice!nano V2, zapewniających komunikację przez bluetooth. Do komunikacji bezprzewodowej są też oczywiście potrzebne baterie. Wybrałem baterie Li-Po 301230, których rozmiar pozwala na zamontowanie ich pod mikrokontrolerami. 

Wszystkie komponenty gotowe, więc czas wziąć lutownicę w dłoń i zabierać się do pracy. Montaż nie był zbyt trudny. Przełączniki da się zamontować tylko w jeden sposób, wyłącznik i reset powinny być zamontowane na wierzchniej stronie płytki, przewody baterii przylutowane do płytki, aby mogła być ona rozłączana przez wyłącznik. 

![Wyłącznik i reset](ferris-sweep/wyl_reset.jpg)
![Bateria](ferris-sweep/bateria.jpg)

Trudność może się pojawić przy montowaniu mikrokontrolera. Ponieważ płytka jest odwracalna, to trzeba na niej połączyć odpowiednie pady, łączące wyprowadzenia mikrokontrolera z resztą płytki. Instrukcje na samym PCB mówią o połączeniu padów od spodu płytki. Zakładają jednak one orientację mikrokontrolera komponentami w dół. Ja zdecydowałem się go zamontować komponentami do góry, z obawy o zbytnie ściśnięcie baterii (a także, według mnie, w ten sposób fajniej wygląda ;) ). W takim wypadku trzeba połączyć pady z wierzchu płytki, pod mikrokontrolerem. 

![Połącznone pady](ferris-sweep/pady1.jpg)

***Komponenty do dołu - pady od spodu płytki, komponenty do góry - pady z wierzchu płytki.***

Można też zwrócić uwagę, aby zlutowane pady "RAW" i "GND" były zgodne z wyprowadzeniami "RAW" i "GND" oznaczonymi na mikrokontrolerze.

![Porównanie RAW i GND](ferris-sweep/pady2.jpg)

Gdy pady już są połączone, a listwy przylutowane, to można się brać za sam mikrokontroler. Jako nóżki można oczywiście wykorzystać specjalne listwy do gniazd precyzyjnych, jednak dodawałyby one sporo niepożądanej wysokości. Lepszym wyjściem jest tutaj użycie drutu. Ja wykorzystałem nóżki rezystorów, które akurat miałem pod ręką. Ważne, żeby wchodziły w gniazdo dość ciasno, te od rezystorów bardzo małej mocy mogą być zbyt cienkie. Mając odpowiedni materiał na nóżki, można położyć mikrokontroler na gnieździe i wkładać druciki po kolei w każde złącze. W internecie można znaleźć historie ludzi, którzy przypadkowo przylutowali mikrokontroler do gniazda, dlatego warto wcześniej gniazdo zabezpieczyć warstwą taśmy malarskiej.

![Montaż mikrokontrolera](ferris-sweep/uc.jpg)
![Gotowa klawiatura](ferris-sweep/gotowa.jpg)

## Software

Klawiatura już gotowa! Żeby jednak nadawała się do pisania, to trzeba ją jeszcze jakoś zaprogramować. W świecie customowych klawiatur dominują 2 oprogramowania: QMK i ZMK. QMK jest starsze i nieco bardziej rozwinięte. Można znaleźć różne, graficzne narzędzia do konfiguracji i inne udogodnienia. W przeciwieństwie do ZMK nie posiada ono jednak obsługi bluetooth, tak więc wybór jest oczywisty.

Domyślnym sposobem instalowania ZMK, opisanym na [ich stronie](https://zmk.dev/docs/user-setup) jest użycie "Github Actions". Jest to metoda, która będzie pewnie preferowana przez większość osób, jednak dla takich indywiduów jak ja, które nie tylko nie mają ochoty korzystać z Githuba, ale i w ogóle nie chcą robić rzeczy w chmurze, istnieje również inny sposób.

### Lokalna kompilacja

***Instrukcje na podstawie [https://zmk.dev/docs/development/setup](https://zmk.dev/docs/development/setup), testowane na 11 stycznia 2024 roku na czystym Ubuntu 23.10***

Do lokalnej kompilacji firmware'u klawiatury wymagane jest środowisko Zephyr. Jego instalacja na Ubuntu 23.10 (i pewnie wszystkich innych debianopochodnych systemach) przebiega następująco:

- Instalacja wymaganych zależności

```bash
sudo apt update
sudo apt upgrade
sudo apt install --no-install-recommends git cmake ninja-build gperf \
  ccache dfu-util device-tree-compiler wget \
  python3-dev python3-pip python3-setuptools python3-tk python3-wheel xz-utils file \
  make gcc gcc-multilib g++-multilib libsdl2-dev libmagic1
```

- Instalacja Zephyr SDK

```bash
cd ~/.local
wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.15.0/zephyr-sdk-0.15.0_linux-x86_64.tar.gz
wget -O - https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.15.0/sha256.sum | shasum --check --ignore-missing
tar xvf zephyr-sdk-0.15.0_linux-x86_64.tar.gz
cd zephyr-sdk-0.15.0
./setup.sh
sudo cp sysroots/x86_64-pokysdk-linux/usr/share/openocd/contrib/60-openocd.rules /etc/udev/rules.d
sudo udevadm control --reload
```

- Utworzenie nowego środowiska wirtualnego w Pythonie, instalacja Zephyr i aplikacji ZMK

```bash
sudo apt install python3-venv
mkdir -p ~/Dokumenty/zmk
cd ~/Dokumenty/zmk
python3 -m venv env
source env/bin/activate
pip install west
git clone https://github.com/zmkfirmware/zmk.git
cd zmk
west init -l app/
west update
west zephyr-export
pip install -r zephyr/scripts/requirements.txt
cd ..
```

Możesz zamienić `~/Dokumenty/zmk/` na jakikolwiek inny katalog, w którym chcesz przechowywać swoją konfigurację. Ważne, żeby teraz nie zamykać terminala, aby nie wyjść ze środowiska wirtualnego. Jeśli go zamkniesz, możesz ponownie wejść do środowiska, ponownie wykonując `source env/bin/activate` z folderu zmk.

Środowisko gotowe, można brać się za klawiaturę.

Domyślna konfiguracja jest dostępna na [Githubie ZMK](https://github.com/zmkfirmware/zmk/tree/main/app/boards/shields/cradio). Można ją pobrać do zmk-config. Z głównego katalogu zmk:

```bash
mkdir -p zmk-config/config
cd zmk-config/config
wget https://raw.githubusercontent.com/zmkfirmware/zmk/main/app/boards/shields/cradio/cradio.keymap
wget https://raw.githubusercontent.com/zmkfirmware/zmk/main/app/boards/shields/cradio/cradio.conf
cd ../..
```

Kompilowanie ZMK po raz pierwszy. Z głównego katalogu zmk:

```bash
cd zmk/app
west build -d ../../build/left -b nice_nano_v2 -- -DSHIELD=cradio_left -DZMK_CONFIG="../../zmk-config/config"
west build -d ../../build/right -b nice_nano_v2 -- -DSHIELD=cradio_right -DZMK_CONFIG="../../zmk-config/config"
cd ../..
cp build/left/zephyr/zmk.uf2 left.uf2
cp build/right/zephyr/zmk.uf2 right.uf2
```

Można już wyjść ze środowiska wirtualnego:

```bash
deactivate
```

W głównym katalogu zmk powinny teraz znajdować się 2 pliki .uf2: "left.uf2" i "right.uf2". Można przystąpić do wgrywania:

1. Podłącz lewą połówkę klawiatury do komputera za pomocą przewodu USB
2. Naciśnij 2 razy przycisk reset na klawiaturze
3. Klawiatura powinna zostać wykryta jako pamięć zewnętrzna. Przenieś plik "left.uf2" do nowo wykrytej pamięci
4. Po chwili pamięć powinna się automatycznie odmontować. Gdy tak się stanie, możesz odłączyć klawiaturę
5. Powyższe kroki powtórz z połówką prawą i plikiem "right.uf2"

Gotowe! Klawiatura powinna już działać!

Wgranie firmware'u na prawą połówkę jest wymagane tylko za pierwszym razem. Po każdej kolejnej modyfikacji nowy plik .uf2 trzeba wgrywać tylko na lewą połówkę. Kompilowanie powinno wyglądać od teraz tak:

```bash
source env/bin/activate
cd zmk/app
west build -d ../../build/left
cd ../..
deactivate
cp build/left/zephyr/zmk.uf2 zmk.uf2
```

Polecam zapisanie sobie tego do skryptu.

## Konfigurowanie ZMK

Konfiguracja ZMK to temat bardzo szeroki i nie byłbym w stanie go tutaj w pełni opisać, nawet gdybym posiadał potrzebną ku temu wiedzę. Z tego powodu będzie to tylko pobieżny przegląd najważniejszych zagadnień. Po więcej informacji zapraszam do [dokumentacji](https://zmk.dev/docs/config).

Na początek ważne jest rozumienie pojęcia warstwy. Można powiedzieć, że warstwa jest zestawem akcji przypisanych do poszczególnych klawiszy. Dla przykładu, w standardowej są klawisze, które wysyłają do komputera znaki: 1, 2, 3, q, w, e. Jest to podstawowa warstwa tej klawiatury, a wysyłanie wymienionych znaków to zestaw akcji przypisany do tych klawiszy. Po przytrzymaniu przycisku Shift zmienia się warstwa i teraz te same klawisze wysyłają znaki: !, @, #, Q, W, E. Innym przykładem zmiany warstwy jest przycisk Alt, który zmienia a, e i o na ą, ę i ó. Zmiana warstwy może być krótkotrwała - tylko na czas trzymania klawisza, lub stała - do czasu jej wyłączenia. Nie musi się też ograniczać do zmiany liter. Warstwa może na przykład zmieniać przycisk zmiany warstwy, co oznacza, że jeden przycisk może na pierwszej warstwie przechodzić do drugiej warstwy, a na drugiej warstwie przechodzić do trzeciej warstwy. 

Warto tutaj zaznaczyć, że porównanie warstw do klawiszy Shift albo Alt nie jest do końca poprawne. Klawisze te są modyfikatorami (dlatego w konfiguracji ZMK nadal istnieją takie klawisze jak Shift). Różnica jest taka, że gdy naciśniemy Shift i "q" to do komputera są wysyłane są Shift i "q". Natomiast jeśli warstwa ZMK zmienia "q" na "1", to do komputera wysyłane jest po prostu "1". Jednakże w szerszym, bardziej praktycznym spojrzeniu, zachowują się tak samo.

Funkcje wszystkich klawiszy zdefiniowane są w pliku zmk-config/config/cradio.keymap. W sekcji "keymap" zdefiniowane są wszystkie warstwy. Można dodawać dowolną ilość nowych warstw. Są one automatycznie, kolejno numerowane, począwszy od zera. Domyślne warstwy są sformatowane w sposób przypominający fizyczny wygląd klawiatury. Kilka z ważniejszych akcji, które można przypisać do klawisza:

- &kp - key press. Wysłanie określonego znaku (włączając w to modyfikatory, takie jak Shift) do komputera. "&kp A" wysyła klawisz "a".
- &ht - hold tap. Wysłanie znaku po przytrzymaniu, lub innego znaku po naciśnięciu. "&ht LSHFT A" wysyła lewy Shift po przytrzymaniu klawisza, lub "a" po naciśnięciu.
- &mo - momentary layer. Przełącza warstwę na czas przytrzymania klawisza, podobnie do klawisza Shift. "&mo 1" przełącza na warstwę 1.
- &lt - layer tap. Połączenie &ht i &mo. "&lt 1 A" zmienia warstwę na 1 podczas przytrzymywania klawisza, lub wysyła "a" po naciśnięciu.
- &to - to layer. Zmienia warstwę na stałe, do czasu wyłączenia, podobnie do Caps Lock. "&to 1" zmienia warstwę na 1.
- &trans - transparent. Wywołuje zachowanie klawisza z niższej warstwy. Co jest niższą warstwą to dłuższa historia.
- &bt - bluetooth. Głównie do przełączania połączonych urządzeń.

W domyślnej konfiguracji można też zauważyć, że niektóre  klawisze są otoczone przez funkcje "HRML()" i "HRMR()". Funkcje te są zdefiniowane na początku pliku i realizują jedną z ważniejszych idei wśród minimalistycznych klawiatur: home row mods. Polega ona na umieszczeniu modyfikatorów na klawiszach, na których palce znajdują się w pozycji spoczynkowej. Na przykład, w domyślnej konfiguracji, środkowy klawisz w pierwszej kolumnie od lewej wysyła lewy Shift po przytrzymaniu i "a" po naciśnięciu. Sugerowałbym zredefiniowanie tych funkcji tak, aby zawierały prawy Alt (RALT). 

Kody wszystkich liter, modyfikatorów i całej reszty można znaleźć w [dokumentacji](https://zmk.dev/docs/codes)

## Dotychczasowe wrażenia 

Obecnie korzystam z tej klawiatury od ponad 3 miesięcy i bez wątpienia mogę powiedzieć, że jest o wiele wygodniejsza od standardowego ANSI albo ISO. Praktycznie całkowicie przestałem korzystać z jakiejkolwiek innej klawiatury. Używam jej nawet do grania w gry (chociaż ta czynność nie zdarza się u mnie zbyt często), zmieniając warstwę na taką, w której wasd jest przesunięte na dscf i w życiu mi się tak wygodnie nie chodziło. Zdecydowanie nie planuję powrotu do standardowej klawiatury.

Nie wszystko jest jednak takie różowe. Nie jest to coś, przynajmniej dla mnie, co od razu działa idealnie i nie trzeba o tym więcej myśleć. Używam jej już dość długo, a mimo to nadal, co jakiś czas modyfikuję jej config. Nawet teraz sobie myślę o tym, że chyba powinienem zmienić czasy hold tap, bo często mi się zdarza, że za szybko używam home row mods i moje naciśnięcia zamiast modyfikator + litera są wykrywane jako litera, litera. Inną niedogodnością jest niezadowalająca stabilność połączenia między połówkami. Nie powinien ten problem występować na biurku, jednak gdy używam klawiatury w łóżku, tak, że leże, albo siedzę pomiędzy dwiema połówkami, to potrafią się czasem dziać z klawiaturą [rzeczy niestworzone](https://paczaizm.pl/dzieja-sie-dziwne-rzeczy-znika-wszystko-z-pulpitu-i-dzieja-sie-rzeczy-niestworzone/). Zbliżenie dwóch części z powrotem do siebie zawsze jednak rozwiązuje problem. Jeśli komuś nie zależy na bluetooth, to sugerowałbym wersję przewodową na QMK, łączoną przez TRRS. 

Mimo wszystko uważam, że warto :) 
